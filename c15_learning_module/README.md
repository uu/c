# C15 Learning Module

## Evaluation
You will be graded on:
- Proper coding style and indentation
- Reasonable naming convention
- Appropriate comments, headers and useful documentation
- Best practices for encapsulation and access control
  
Remember: Functionalization is your friend! The more you can functionalize (writing small functions that are reusable), the better! 

## Instructions

You are going to revisit your Binary Sort function from earlier in the semester. Again, you are welcome to adapt your own work, or you can start from scratch. __Be sure to cite any sources you use, even if you are the source!__

## (100 pts) Generic Binary Sort 

You are going to write a function with the following parameters: 

```cpp
Template <typename Type> 
void binarySort(Type *array, int size); 
```

This function will utilize a recursive binary search method to sort the array that is passed in. You need to __demonstrate__ that this works for arrays of `int`, `float` and `string`. The demonstration needs to be original work. 

Hint: "demonstrate" means a well documented output and comments that show your code is working to the specification. This means working on a sizeable array with interesting contents.

__REMEMBER__: It is a serious academic integrity violation to copy code electronically or share your code electronically with another student! 