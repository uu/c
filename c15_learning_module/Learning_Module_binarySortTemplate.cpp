#include <iostream> 

using namespace std; 

//Print array function
template <typename T>
void print(T *arr, int s); 

//Sorting method
template <typename T> 
void binarySort(T *array, int aSize);

//Insert element into a position within an array
template <typename T>
void insert(T* &array, int aSize, T element, int pos);


// Binary search to find key inside array
// Input: array - in ascending order
//        key - value we are looking for in array
//        start - location in array where we should be looking
//        end   -  last possible location to look
// Return ether: positive value - location of a key element in array
//               negative value - insertion point where key element should go
// From learning module 10 
template <typename T> 
int BS(T array[], T key, int start, int end);

// Verification that both of the arrays are te same
// Input: 2 arrays
// Return ether: True - arrays are equal
//               False - arrays are not equal
template <typename T> 
bool verifyArray(T arr1[], T arr2[], int aSize, int aSize1){
    //If the sizes are not equal to each other
    //They cant be the same
    if(aSize != aSize1){
        return false;
    }

    //Add sorting function here
    binarySort(arr1, aSize);
    binarySort(arr2, aSize1);

    for (int i = 0; i < aSize; i++){
        if (arr1[i] != arr2[1]){
            return false;
        }
    }
    return true;
}

int main(){
    
    int values[] = {1,2,3,4,5,6,6,6,6,6,7,8,8,10,12,15};
    int values1[] = {1,2,3,4,5,6,6,6,6,6,7,8,8,10,12,15};
	
    int vSize = 16;
    int vSize1 = 16;

    cout << "Calling binary search:" << endl;
    int result = BS(values,9,0,vSize-1);
    cout << "Return of BS: " << result << endl;

    int t = verifyArray(values, values1, vSize, vSize1);
	binarySort(values, vSize); 

	
    if (t = 1){
        cout << "The arrays are the same" << endl;
    }
    else {
        cout << "The arrays are not the same" << endl; 
    }

    return 0; 
}

template <typename T> 
int BS(T array[], T key, int start, int end){
    //Index of mid point in the array
    int mid = ((end - start ) / 2) + start;
    cout << "BS(array,"<<key<<","<<start <<"," << end <<") mid: " << mid << endl;

    //Base Case        
    if(key == array[mid]){
        // - found it! return mid
        return mid;
    } 
    else if(end < start) {
        // - didn't find it, return insertion point
        return (start + 1) * -1;
    } 
    else if (key < array[mid]) {
        // determine if you need to go left or right
        // Recursive call to take left path
        return BS(array,key,start,mid-1);
    } 
    else if (key > array[mid]) {
        // Recursive call to take left path
        return BS(array,key,mid+1,end);
    } 
    else {
        cout << "ERROR! Reached end of BS!" << endl;
        return -1;
    }
}


template <typename T>
void insert(T* &array, int aSize, T element, int pos){
    //Init dynamic array
    T* temp = new T[aSize + 1]; 

    //copies from beginnig to pos
    for (int i = 0; i < pos; i++){
        temp[i] = array[i]; 
    }
    
    temp[pos] = element;  

    //Copies other side of array from pos
    for (int i = pos; i < aSize; i++){
        temp[i + 1] = array[i]; 
    }
    
    delete[] array; 
    array = temp; 
}


template <typename T>
void print(T *arr, int s){
	for(int	i = 0; i < s; i++){
		cout << arr[i] << " "; 
	}
}

template <typename T> 
void binarySort(T *array, int n){
  // Define a temporary working space (this will become your sorted array
    T *temp = new T[1];
    unsigned int aSize = 1; //size var

  // Initilize your temp space with the first value of array (array of size 1 is sorted
    temp[0] = array[0]; 
// For every element in array, find its insertion point in the temp array
    for (int i = 1; i < n; i++){
        int pos = BS(temp, array[i], 0, n); 
        // insert element into insertion point
        insert(temp, n, array[i], pos); 
    }
// Deal with the temp and array (either copy or pointer swap)
    delete[] array; 
    array = temp;
}

//Insert function - complete 
//Verfiy function - complete
//binary search - complete
//binary sort - complete
//Print function - complete
