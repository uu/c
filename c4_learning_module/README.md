# C4 Learning Module

The goal is to get continue practicing the development of classes.

## Evaluation
You will be graded on:
- Proper coding style and indentation
- Reasonable naming convention
- Appropriate comments, headers and useful documentation
- Best practices for encapsulation and access control

## Instructions

You are going to make three classes to represent three different contexts of classes.  
  
* One class will be an object that is around you right now. This is a __physical object__. You should focus on what attributes might be useful to code , and what attributes are useful for differentiating one object from another.  
* One class will be an __abstract idea__. Similar to how I mentioned mood or argument in the video. This is something that can be differentiated, but still has a behavior or verb associated with it.  
* One class will be a __CS construct__. This will be something that is 'virtual' but still might represent a physical object or abstract idea. An example would be a tweet or a netflix show. Something that has a clearly defined boundary, with clearly defined attributes and behaviors.  

If you need help brainstorming, use the Discord chanel!

Each class should have a __specific context__. I would suggest documenting what that context is. I don't want to see 20 different attributes of a Lamp, but I also don't want to see a bed that only has one behavior. I want to see a reasonable set of attributes and behaviors (more than 2 less than 8). __Only one behavior, like a `print()` method, can have _only_ `cout` statements.__ 

For each of the above classes you will need to develop the following:
* Expanded UML diagrams 
  * I would suggest doing this first, as it will easily translate into the code
  * You are welcome to use whatever tools you like, you can submit a picture/scan of a piece of paper, you can submit a word or pdf, you can use any number of online resources. Be creative in how you approach this, but don't take too much time figuring it out.
* Separate .h and .cpp files that are fully documented
  * These can all be in the same folders, your `headers` folder will have three .h files and your `source` will have four .cpp files (one for `int main()`)
  * Use proper access control. This means you will likely need to use setters/getters, these don't count as 'behaviors'.
* Example use in a single `int main()`
  * Each class should have at least 3 objects created (at least three constructors called)
  * This is kind of like a _unit test_, something that confirms that the classes operate in a way that is expected. This means you should test __all__ of the methods (this means calling them at least once and showing they are working).