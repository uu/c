__UML Diagrams__

1. Physical 

[Properties]
    - bool haswater
    - bool hotwater
    - bool onoff
    - bool heating

|--------------------------------------|

[Behaviors]                 
    - void boilingWater()
    - void makingRamen()


2. Abstract

[Properites]
    - bool happy
    - bool disgust
    - bool mad
    - bool confused

|--------------------------------------|

[Behaviors] 
    - feelBetter()
    - feelWorse()


3. CS Construct 

[Properties]
    - string tweets
    - string username
    - int likes
    - int retweet

|--------------------------------------|

[Behaviors]
    - void isTweeting()
    - void activity()
