#ifndef MOOD_H
#define MOOD_H


/*
Creating mood class
This class will have 4 different emotions 
1. Happy
2. Sad
3. Mad
4. Confused

*/ 

class Mood{

    //Varibles
    private: 
        
        bool happy; //Are you happy (True or False) 
        bool disgust; //Are you disgusted (True or False)
        bool mad; //Are you mad (True or False)
        bool confused; //Are you confused (True or False)

    public: 

    Mood(); //Default 

    //You are happy
    //You are disgused
    //You are mad
    //you are confused

    //Arg. Constructor
    Mood(bool h, bool d, bool m, bool c);
    
    

    //Getters and Setter
    bool getDisgust(); 
    bool getMad();
    bool getHappy(); 
    bool getConfused(); 

    //Print function to output 
    void print(); 



};

#endif 
