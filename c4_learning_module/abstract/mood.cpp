#include "mood.h"
#include <iostream> 

using namespace std; 

//Default Constructor 
//Sets default 
Mood::Mood(){
    happy = false; //Are you happy (True or False) 
    disgust = false; //Are you sad (True or False)
    mad  = false; //Are you mad (True or False)
    confused = false; //Are you conufused (True or False)
}


//args, sets 
//happy to h
//sad to s
//mad to h
//confused to c
Mood::Mood(bool h, bool d, bool m, bool c){
    happy = h; 
    disgust = d;  
    mad = m; 
    confused = c; 
}




//Getters and setters
bool Mood::getHappy(){
    return happy; 
}

bool Mood::getDisgust(){
    return disgust; 
}

bool Mood::getMad(){
    return mad; 
}

bool Mood::getConfused(){
    return confused; 
}
 
//Print Statement
//Outputs statment based off true or false value
void Mood::print(){
    
    if(happy == 1){
        cout << "You are happy :)" << endl; 
    } 
    
    else {
        cout << "You are sad :(" << endl; 
    }

    if(disgust == 1){
        cout << "You are disgusted" << endl; 
    } 
    
    else {
        cout << "You are satisfied" << endl; 
    } 

    if(mad == 1) { 
        cout << "You are mad >:(" << endl; 
    }
    
    else {
        cout << "You are not mad :)"  << endl;
    }

    if(confused == 1) {
        cout << "You are confused o_o" << endl; 
    }

    else { 
        cout << "You understand :D" << endl; 
    } 

}

