#include <iostream> 
#include <string>
#include "tweet.h"
using namespace std; 

        //Default Constructor
        //Default settings for tweet
        Tweet::Tweet(){
            tweets = "NA"; 
            username = "NA";
            likes = 0;
            retweet = 0; 
        }

        // args, sets the username and tweet
        Tweet::Tweet(string t, string u){
            tweets = t; 
            username = u;
            likes = 0;
            retweet = 0; 
        }
        
        //sets username, tweets, likes, and retweet
        Tweet::Tweet(string u, string t, int  l, int r){
            username = u;
            tweets = t; 
            likes = l;
            retweet = r; 
        }
        
        //Getters and Setter
        
        //returns likes
        int Tweet::getLikes(){
            return likes; 
        }
        //returns retweet
        int Tweet::getRetweet(){ 
            return retweet; 

        }

        //Print Function to output info
        void Tweet::print(){
            cout << "Twitter username:  " << username << endl;    
            cout << "Tweet: " << tweets << endl;  
            cout << "Number of likes: " << likes << endl; 
            cout << "Number of retweets " << retweet << endl; 
        }
