//inclusion guards
#ifndef TWEET_H
#define TWEET_H

#include <string>
using namespace std; 

//Naming class: Tweet
class Tweet {
    //Defining variables 
    private: 
        string tweets;
        string username;
        int likes;
        int retweet; 
    
    //Behaviors 
    public: 
        //Default Constructor
        //Constructors are always going to be public
        Tweet(); 
        
        // args, sets tweet and username
        Tweet(string t, string u);
        // args sets likes and retweets 
        Tweet(string t, string u, int l, int r); 
        
        //Getters and Setter
        //gets amount of likes
        int getLikes();
        
        //gets about of retweets 
        int getRetweet();
        
        //Print function 
        void print(); 

};

#endif
