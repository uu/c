#include <iostream> //Printing to the terminal
#include "tweet.h" //including tweet.h

using namespace std;  

int main(){
    
    //creates tweet with: 
    //Username: Saned
    //Tweet: "Hello this is my first tweet
    //Number of likes
    //Number of retweets
    Tweet t1("Saned", "Hello this is my first tweet", 4000, 500);
    

    //Calls print 
    t1.print();
}
