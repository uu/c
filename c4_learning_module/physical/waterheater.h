#ifndef WATERHEATER_H
#define WATERHEATER_H


/*
Creating a water heater class
* A water heater can heat water
* A water heater can contain water / be empty
* A water heater can be turned on or off
*/


class Waterheater {

    private: 
        
        bool hotwater; //Water is hot (True or False) 
        bool haswater; //Water Heater contains water (True or False)
        bool onoff; //The water heater is turned on (True or False)
        bool heating; //The water heater is heating water (True or False)
    public: 

    Waterheater(); //Default 

    //Water is not hot
    //Water does not contain water
    //Water Heater is turned off
    //Water heater is off

    //Arg. Constructor
    Waterheater(bool hotcold, bool water, bool power, bool heat);
    
    

    //Getters and Setter
    bool getWater(); 
    bool getPower(); 
    bool getHot(); 
    bool getHeat(); 

    //Print function to output 
    void print(); 



};

#endif 
