#include "waterheater.h"
#include <iostream> 

using namespace std; 

//Default Constructor
Waterheater::Waterheater(){
    hotwater = false; //Water is hot (True or False)
    haswater = false; //Water Heater contains water (True or False)
    onoff = false; //The water heater is turned on (True or False)
    heating = false; //The water heater is heating water (True of False)
}


//args, setting hotwater, haswater, onoff, and heating 
Waterheater::Waterheater(bool hotcold, bool water, bool power, bool heat){
    hotwater = hotcold;
    haswater = water; 
    onoff = power; 
    heating = heat; 
}




//Getters and setters
bool Waterheater::getWater(){
    return haswater; 
}

bool Waterheater::getPower(){
    return onoff; 
}

bool Waterheater::getHot(){
    return hotwater; 
}

bool Waterheater::getHeat(){
    return heating; 
}
 
//Print Statement
//Outputs statment based off true or false value
void Waterheater::print(){
    if(heating == 1){
        cout << "The water heater is heating water" << endl; 
    } 
    
    else {
        cout << "The water heater is off" << endl; 
    }

    if(hotwater == 1){
        cout << "The water heater has hot water" << endl; 
    } 
    
    else {
        cout << "The water heater has cold water" << endl; 
    } 

    if(haswater == 1) { 
        cout << "The water heater has water" << endl; 
    }
    
    else {
        cout << "The water heater does not contain water" << endl;
    }

    if(onoff == 1) {
        cout << "The water heater is on" << endl; 
    }

    else{ 
        cout << "the water heater is off" << endl; 
    } 

}

