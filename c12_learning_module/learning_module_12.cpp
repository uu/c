#include <iostream>

using namespace std; 



// Returns memory location of the smallest item in an array of ints
// input: array - collection of integers that will be searched
//        aSize - Total number of elements in array that need to be searched.
// Output: memory location of the smallest item.
int* findSmallest(int* arr, int aSize){
	
	int *min = &arr[0]; 
	for(int i = 0; i < aSize; i++){
		if (arr[i] < *min){
			min = &arr[i]; 
		}
	}

	return min;  
}



//Function to reverse array using pointer math 
void recursiveSwitch(int * start, int * end){
    if(start >= end){ 
        return;
    }
    else{
        int t;  // temp varible
        t = *start;
        *start = *end;
        *end = t;

        recursiveSwitch(start + 1, end - 1);
    }

}


//Function to print array
void printArray(int arr[], int size){
	for(int i = 0; i < size; i++){
		cout << arr[i] << " ";  
	}
	cout << endl;
}


int main(){
	const int SIZE = 5; 
	int arr[SIZE] = {2, 4, 6, 8, 10}; 
	int arr1[SIZE] = {1, 3, 5, 7, 9};
	
	cout << "Cross checking memory address: " << &arr[0] << endl;  	
	int* min = findSmallest(arr, SIZE); 
	cout << "The memory address of the smallest element is: " << min << endl;

	

	//Print original array 
	printArray(arr, SIZE); 
	
	//Recursive call to reverse array
	recursiveSwitch(arr, arr + SIZE - 1);
	
	//Print reversed array
	printArray(arr, SIZE); 

	
	//Recusive call for array 2
	printArray(arr1, SIZE);
	recursiveSwitch(arr1, arr1 + SIZE - 1);	
	printArray(arr1, SIZE); 

	
	//The name of an array is a pointer to the first element of the array

}





/* 
		end*

1 2 3 4 5
^		x

*start = end* 

5 = end* - 1

*/ 
