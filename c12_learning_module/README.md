# C12 Learning Module

Take what we are going to do in class and build on it.

## Evaluation
You will be graded on:
- Proper coding style and indentation
- Reasonable naming convention
- Appropriate comments, headers and useful documentation
- Best practices for encapsulation and access control
  
Remember: Functionalization is your friend! The more you can functionalize (writing small functions that are reusable), the better! 

## Instructions

The following should be straight-forward algorithms, but I want you to implement them without using traditional for loop / array index operations. You are welcome to use `[]` operators in `main`, but your functions should only be using pointers.

## (50 pts) Part 1: Reverse Array Function

Develop and demonstrates a function that recursively reverses an array of `int`s using the following recursive function declaration:    

`void recursiveSwitch(int * start, int * end); `  

Show that this function works correctly with different sized arrays, in particular arrays of even and odd values.
Demonstrate that it is a non-destructive function (i.e. that if you reverse an array, you can reverse it again to get the original array).

## (50 pts) Part 2: Smallest Value Function

Develop and demonstrate a function that finds the smallest element in an array of `int`s.

The function must follow the following declaration:
```cpp
// Returns memory location of the smallest item in an array of ints
// input: array - collection of integers that will be searched
//        aSize - Total number of elements in array that need to be searched.
// Output: memory location of the smallest item.
int* findSmallest(int* array, int aSize);
```

To demonstrate: create an array of at least 10 different values. 
Use the function to replace the smallest number with a value one greater (I would suggest using the `++` operator).
Keep doing this replacement until all values are the same. You probably should develop additional functions to support.
To be clear: the addition of one should happen outside `findSmallest()`

**+5 pts EX: Write findSmallest recursively.**

__REMEMBER__: It is a serious academic integrity violation to copy code electronically or share your code electronically with another student! 
