#include <iostream>
#include <string> 

using namespace std; 

bool anagramfinder(string s1, string s2) { 

     // creates array named count and inilizes it with 0 
    int count[0];   
  
    // For each character in the strings, increment count in 
    // the corresponding count array
    
    int n1, n2;

    n1 = s1.length(); 
    n2 = s2.length(); 

    //Sorting array
    


    //if the length of string s1 does not equal the length of s2 
    //return false
    if(n1 != n2){
        return false; 
    }
    



    for (int i = 0; i < n1; i++){
        //compares strings with sorted letters
        if(s1[i] != s2[i]){ 
            return false; 

        }
    }

    return true; 
}

int main() {
    
    //defines varible anagram
    bool anagram; 
    
    //Assigns our two strings
    string s1 = "silent"; 
    string s2 = "listen"; 
    
    //Stores our function call anagramfinder with our 2 strings into one varible anagram
    anagram = anagramfinder(s1, s2); 

    
    //if anagram is true (or 1) print cout statment 
    //else print not an anagram
    if (anagram == 1){ 
        cout << "This is an anagram"; 
    }
    else{
        cout << "This is not an anagram"; 
    }
    
    return 0; 
}

