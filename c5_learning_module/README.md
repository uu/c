# C5 Learning Module

The goal is to get continue practicing the use of classes through experimentation with `string`.

## Evaluation
You will be graded on:
- Proper coding style and indentation
- Reasonable naming convention
- Appropriate comments, headers and useful documentation
- Best practices for encapsulation and access control

## Instructions

This LM has two parts:
1. String experimentation
2. String application

For this LM, I would highly suggest referencing http://www.cplusplus.com/reference/string/string/append/   
Be sure to document your references and do your own work!

### Part 1: String Experimentation (10 pts each)

You are going to pick __four__ methods from the following methods of the string class and show how they can be utilized.

* `insert()`
* `erase()`
* `replace()`
* `substr()`
* `compare()`
* `find()`

For each of these methods __fully demonstrate__ their use. This probably should include some of the overloaded methods. And you should probably call the methods a number of times.

### Part 2: String Application (60 pts, +20 extra)

You have a choice on this section. Pick one of the following applications. If you complete one, you will get full points for this section (60pts). If you complete both, you will get up to 20pts extra. Grading for the extra credit attempt will be much more stringent.

#### Option 1: Anagram Checker

Write and demonstrate a function that checks to see if two strings are anagrams. An anagram is a word that contains that same letters as another word, but their order can be different. For this exercise you should ignore spaces as part of the calculation.  

Example: 'i am lord voldemort' is an anagram of 'tom marvolo riddle'.  

You decide if you want to differentiate capital letters (but document either way).

#### Option 2: Word Reversal

Write and demonstrate a function that reverses every word in a string. Preserve the order of the words and punctuation (don't 'just' reverse the string).  

Example: "CS2 is the best class ever!" should be: "2SC si eht tseb ssalc reve!"  