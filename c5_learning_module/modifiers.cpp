/*### Part 1: String Experimentation (10 pts each)

You are going to pick __four__ methods from the following methods of the string class and show how they can be utilized.

* `insert()`
* `erase()`
* `replace()`
* `substr()`
* `compare()`
* `find()`
For each of these methods __fully demonstrate__ their use. This probably should include some of the overloaded methods. And you should probably call the methods a number of times.
*/ 

#include <iostream>
#include <string>

using namespace std; 

int main(){ 
    
    //Sets string with phrase
    string cs = "CS is cool";
    
    //Finds word cool and replaces it with fun
    cout << cs.replace(cs.find("cool"), 4, "fun") << endl;   
    
    //goes to position 3 and outputs 2 characters
    cout << cs.substr(3, 2) << endl;

    //goes to position 3 in cs string and deletes 2 characters after
    cout << cs.erase(5, 4) << endl; 
    



}
