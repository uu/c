#include "../headers/Album.h"
#include <string> 
#include <iostream>
#include <ctime> 

using namespace std; 

Album::Album() 
{
    album_title = "NA"; 
    album_artist = "NA"; 
    maxSongs = 0; 
}

Album::Album(string title, string artist)
{
    album_title = title; 
    album_artist = artist; 
    maxSongs = 0; 
}

//Adds tracks to album 
//Name of track and length
bool Album::addTrack(string title, float length)
{
    if (maxSongs < MAX_NUM_SONGS){
        songs[maxSongs] = title; 
        duration[maxSongs] = length; 
        maxSongs++; 
        return true; 
        }
    else {
        return false; 
    }
}

//Getting max album length
//Takes all lenghts within the duration and adds them together
int Album::getAlbumLength() const 
{
    float t = 0; //init 
    for (int i = 0; i < maxSongs; i++){ 
        t += duration[i];  
    }
    return t; 
}

//Shuffling
 string Album::shuffleAlbum(int n) const{
     bool used[MAX_NUM_SONGS] = {false};
     string output;

     //used to pick songs n
     for (int i = 0; i < n; i++) {
         //while true
         //keeps going through index until all songs have been randomized
         while (true) {
             int index = rand() % maxSongs;
             //if false means index has not been used yet
             if (!used[index]) {
                 output += "[" + to_string(i + 1) + "] " + songs[index] + " " + time(duration[index]) + " , ";
                 used[index] = true;
                 break;
             }
         }
     }

     return output;
 }


string Album::time(int m) const
{
    int minutes = (m / 60);
    int seconds = (m % 60);
    string output = "";


    output += "(" ;
    if(minutes < 10){
        output += "0";
    }
    output += to_string(minutes) + ":";
    if(seconds < 10){
        output += "0";
    }

    output += to_string(seconds) + ")";


    return output;
}

string Album::getAlbumLength_string() const //Returns total length of ablum in (ms:ss)
{

    return time(getAlbumLength()); 

/*
    int length = getAlbumLength();
    int minutes = (length / 60); 
    int seconds = (length % 60);
    string output = "";
    

    output += "(" ; 
    if(minutes < 10){
        output += "0"; 
    }
    output += to_string(minutes) + ":";  
    if(seconds < 10){
        output += "0"; 
    }

    output += to_string(seconds) + ")"; 
    

    return output; 

*/
}


void Album::print() const
{
    cout << "Album Title: " << album_title << endl; 
    cout << "Album Artist: " << album_artist << endl; 
    //Not sure about this
    cout << "Album Length: " << getAlbumLength() << endl;  
    cout << "Album Length: " << getAlbumLength_string() << endl;
    cout << shuffleAlbum(maxSongs) << endl; 
    
}









