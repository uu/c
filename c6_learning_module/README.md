# C6 Learning Module

At this point you should know everything you need to know about making classes. You have all the tools, let's start putting them to the test.

## Evaluation
You will be graded on:
- Proper coding style and indentation
- Reasonable naming convention
- Appropriate comments, headers and useful documentation
- Best practices for encapsulation and access control

## Instructions

This is a two part learning module, you will get started with the basic framework here then build on it for LM7.   

It may seem like you should do both at the same time, but I want you to keep them __separate__. Once you finish this one, submit and leave it alone. Then work on the other. If you make changes to your structure while working on LM7, that is fine, leave this as is.

Over LM6 and LM7 you are going to develop an `Album` class. This class is going to represent what would be stored in the backend of a Spotify or iTunes service. 

### First Step: Make Album

You are going to develop a new class named `Album`. This class will have (at a minimum) the following attributes and features:
* Attributes:
  * Album Title
  * Artist Name
  * Array of song names (Track Listing)
  * Array of track lengths (decide on a unit)
  * At most there can be 20 songs on an album.
* Behaviors:
  * Default and at least one argumented constructor
  * Appropriate getters for attributes
  * `bool addTrack(string name, type duration)` - adds track to album. You decide type on duration. Make sure you document what this is.
  * `getAlbumLength()` - method that returns total length of album matching whatever units / type you decide.
  * `getAlbumLength_string()` - method that returns total length of album in `mm:ss` format string. I would suggest using your other getLength function here.
  * `string shuffleAlbum(int n)` - method that returns a formatted string made up of n random tracks from the album. 
    * The string will be in the format `[1] track_name (mm:ss), [2] track_name (mm:ss)...`  
  * Well formatted `print()`
* Use appropriate private/public settings
* Use appropriate `const` application
* The only method of `Album` that should have any `cout`s in it is `print()`. No other `cout` should be used __inside__ Album!
  * You are welcome, and encouraged to develop with `cout`s in your methods as a testing / development cycle. Your final product and design should not rely on those `cout`s

In conjunction with developing this class, you should show the functionality of the class in `int main()` by constructing at least two albums, each with at least 5 tracks. Show off that the methods you have created work appropriately. 

Remember: If you find yourself copy/pasting code from one area of your work to another, there is likely a better way! Functionalize! 

#### Extra Credit

You are welcome to implement more functionality. Be very clear about what is deviating from the baseline. Only exceptional work will be recognized.