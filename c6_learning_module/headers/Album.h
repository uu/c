#ifndef ALBUM_H
#define ALBUM_H

#include <string> 
#include <iostream> 


using namespace std; 

class Album {
    
    public: 
        //Sets max number of songs 
        static const int MAX_NUM_SONGS = 20; 
    private: 
        string album_title; //album title
        string album_artist; //ablum artist
        string songs[MAX_NUM_SONGS]; //array of song names
        float duration[MAX_NUM_SONGS]; //array of song length
        int maxSongs; 

    public:
        string time(int m) const;  

    public: 
        Album(); 
        
        Album(string title, string artist); 

        bool addTrack(string title, float duration); 

        int getAlbumLength() const; //Returns total length of album 
        
        string getAlbumLength_string() const; //Returns total length of ablum in (ms:ss)
        
        string shuffleAlbum(int n) const; //will shuffle 
        
        string getAlbumtitle() const; 

        string getAlbumartist() const; 


        void print() const; //Print function 

}; 



#endif 
