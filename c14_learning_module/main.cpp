#include "gradebook.h" 
#include <string>
#include <iostream> 

using namespace std; 

int main(){
	Gradebook g1("Learning Module"); 
	
	//What do we need to test
	//Copy constructor
	//addGrade
	//getStats
	//Print
	
	g1.addGrade(66); 	
	g1.addGrade(63); 	
	g1.addGrade(43); 	
	g1.addGrade(65); 	
	g1.addGrade(87); 	
	
	cout<< g1.getStats() << endl; 
	
	g1.print(); 

	Gradebook g2(g1); 
	g2.setLabel("Exams"); 

	g2.addGrade(66); 	
	g2.addGrade(63); 	
	g2.addGrade(43); 	
	g2.addGrade(65); 	
	g2.addGrade(87); 	
	
	cout << g2.getStats() << endl; 

	g2.print(); 
}
