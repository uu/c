#include "gradebook.h" 
#include <string>
#include <iostream> 

using namespace std; 

Gradebook::Gradebook(){
	cout << "Default constructor" << endl; 
	label = "NA";
	gradeSet = NULL; 
	numGrades = 0;  
}

Gradebook::Gradebook(string label){
	this->label = label; 
	numGrades = 0; 
	gradeSet = new float[0];
}
Gradebook::Gradebook(string label, float* gradeSet, int numGrades){
	cout << "Arg Constructor" << endl; 
	this->label = label; 
	this->numGrades = numGrades; 
	this->gradeSet = new float[0]; 
}

Gradebook::~Gradebook(){
	delete[] gradeSet; 
}

//Copies elements from one array and creates a new one
Gradebook::Gradebook(const Gradebook &other){
	this-> label = other.label;
	this-> gradeSet = new float[other.numGrades]; 
	this-> numGrades = other.numGrades; 


	for(int i = 0; i < other.numGrades; i++){
		this-> gradeSet[i] = other.gradeSet[i]; 
	}
}

float Gradebook::getGrade(int index){
	return gradeSet[index]; 
}


void Gradebook::setLabel(string newLabel){
	label = newLabel; 
}

//Stat calcs

float Gradebook::calcAvg() const{
	float sum = 0.0; 
	float average = 0.0; 
	
	for(int i = 0; i < numGrades; i++){
		sum += gradeSet[i]; 
	}
	average = sum / numGrades;
	return average; 	
}

float Gradebook::calcMax() const{
	int max = gradeSet[0];  

	for(int i = 0; i < numGrades; i++){
		if(max < gradeSet[i]){
			max = gradeSet[i]; 
		}
	}
	
	return max; 
}

//http://www.cplusplus.com/forum/beginner/138770/
float Gradebook::calcMedian() const{
	int i, j;
	int median = gradeSet[0]; 
	for (i = 0; i < numGrades; i++){
		for (j = 0; j < numGrades - i - 1; j++){
			if (gradeSet[j] < gradeSet[j + 1]){
				int temp = gradeSet[j];
				gradeSet[j] = gradeSet[j + 1];
				gradeSet[j + 1] = temp;
			}
		}
	}
	if (numGrades % 2 == 0){
		median = (gradeSet[i/2] + gradeSet[i/2-1])/2.0;
	}
	else{
		median = gradeSet[i/2];
	}
	return median; 
 }

float Gradebook::calcMin() const{
	int min = gradeSet[0];
	
	for(int i = 0; i < numGrades; i++){
		if(min > gradeSet[i]){
			min = gradeSet[i]; 
		}
	}
	
	return min; 
}

void Gradebook::allocatePlusOne(){
	float* temp = new float[numGrades + 1]; 
	
	//Copy contents
	for(int i = 0; i < numGrades; i++){
		temp[i] = gradeSet[i]; 
	}
	delete[] gradeSet;
	gradeSet = temp;
	numGrades++; 
}

void Gradebook::addGrade(float grade){
	allocatePlusOne();
	gradeSet[numGrades - 1] = grade; 
}

//Output 
//Average
//Max
//Min
//Median
string Gradebook::getStats(){
	string output = ""; 

	output += "Stats for " + label + " grades: \n" + 
	"Average: " + to_string(calcAvg()) + "\n" + 
	"Max: " + to_string(calcMax()) + "\n" + 
	"Min: " + to_string(calcMin()) + "\n" + 
	"Median: " + to_string(calcMedian()); 	
		

	return output; 	

}


int Gradebook::getnumGrades(){
	return numGrades; 
}

void Gradebook::print() const{
	cout << "[";  
	for(int i = 0; i < numGrades - 1; i++){
		cout << gradeSet[i] << ", "; 
	}  
	cout << gradeSet[numGrades - 1]; 
	cout << "]" << endl; 
}
