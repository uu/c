#ifndef GRADEBOOK_H_
#define GRADEBOOK_H_

#include <string> 

using std::string; 


class Gradebook{
	private:
		string label; //Category of grades
		float* gradeSet; //collection of grades
		int numGrades; //size of gSet

	public: 

		//Default Constructor 
		Gradebook(); 
		
		//lab - category
		//set - collection
		//sizeGrades - size of grades
		Gradebook(string lab, float* gradeSet, int sizeGrades); 
		
		//For label of gradebook
		Gradebook(string label);
		
		//Deep copy constructor
		Gradebook(const Gradebook &other); 
		
		//returns single grade
		float getGrade(int index); 
		

		//Deconstructor
		~Gradebook(); 

		//Adds grade to the new element of the array
		void addGrade(float grade); 
			
		//Set name
		void setLabel(string newLabel); 
		
		//Gets stats and formats it
		string getStats(); 
		
		int getnumGrades(); 

		//Print function
		void print() const; 	
	
	
	
	
	private: 
		void allocatePlusOne(); //Adds one grade to gSet array
		
		float calcAvg() const; //Calculate average
		float calcMin() const; //Calculate min
		float calcMax() const; //Calculate max 
		float calcMedian() const; //Calculate median 
		
};

#endif
