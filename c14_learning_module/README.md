# C14 Learning Module

## Evaluation
You will be graded on:
- Proper coding style and indentation
- Reasonable naming convention
- Appropriate comments, headers and useful documentation
- Best practices for encapsulation and access control
  
Remember: Functionalization is your friend! The more you can functionalize (writing small functions that are reusable), the better! 

## Instructions

__YOU SHOULD FULLY COMPLETE LM13 BEFORE STARTING THIS ONE__

We are going to build on the work you started for LM13.

I want you to make a `Grades` class that holds the dynamic arrays of grades. The functionality is going to be largely the same as your LM13, just with a different implementation.

### (60 pts) `Grades` Class Development

You are going to need to develop a class called `Grades` with the following requirements
* Attributes of `Grades`:
  * `string label` - What category you would call these associated grades
  * `float* gSet` - a collection of grades, dynamically allocated array
  * `int numGrades` - current size of gSet
* Methods of `Grades`:
  * Default, argumented and copy constructors 
  * appropriate destructor
  * appropriate getters (like an ability to access a single grade)
  * `void addGrade(float grade)` - adds one grade to the gSet array
  * Statistic Methods
    * `float calcAvg()` - returns the current average of the grades
    * `float calcMedian()` - returns median grade
    * `float calcMin()` - returns the lowest grade
    * `float clacMax()` - returns the highest grade
    * `string getStats()` - return a formatted string of the above information
    * these calculations are not 'getter's
    * you will need a for loop likely
  * `void print()` - Output contents to screen
  
### (40 pts) `Grades` demonstration

Create a dynamic array of `Grades`.

Fill those `Grades` appropriately.  
Report back on the statistics of each grade as well as the overall grade in the class.  
You can use your LM13 as a framework.  

You will need to keep functionalizing.

Hint: if you want to be fancy (not required, just a foreshadow to stuff we will talk about next week):
```cpp
    Grades** gradeBook; //Double pointer (pointer to an array of pointers)
    gradeBook = new Grades*[3]; // Create an array of Grades pointers
    gradeBook[0] = new Grades("Learning Module"); 
    //gradeBook[0] is a pointer that is now equal to a new Grade object
    gradeBook[1] = new Grades("Exams");
    gradeBook[2] = new Grades("Final Project");

    //to use them:
    gradeBook[0]->addGrade(95.5);

    //This is analogous to:
    //  | gradeBook[0] |
    //|*(gradeBook_0_pointer) | 
    *(  *( gradeBook+0 )      ).addGrade(95.5);

    //...
    // Deleting takes a couple of steps
```

### (up to 15 pts extra) Improvements

There are lots of ways to improve this beyond these specifications. If you feel you are deserving of extra credit, cite what you did and how you did it. 
These will need to be new and different from anything you introduced in LM13.