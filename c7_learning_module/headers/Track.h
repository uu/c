#ifndef TRACK_H
#define TRACK_H

#include <string> 
#include <iostream> 

using namespace std; 

class Track {

    private:
        string title;
        int number;
        int duration; 
    public:
        Track();
        Track(string t, int n, int d); 
        
        string getTitle() const; 
        int getNumber() const;
        int getDuration() const; 
}; 



#endif 
