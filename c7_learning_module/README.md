# C7 Learning Module

We are going to build on the work you started on Tuesday

## Evaluation
You will be graded on:
- Proper coding style and indentation
- Reasonable naming convention
- Appropriate comments, headers and useful documentation
- Best practices for encapsulation and access control

## Instructions

This is the follow up to C6 Leaning Module. __You need to finish LM6 FIRST.__

You are going to take your work from LM6, copy it into here and modify it to contain a new compositional class: `Track`.

As you work on this portion, it is reasonable that you might want to go back and change LM6. I would urge you to leave LM6 as it is. Make your modifications to LM7.

### Second Step: Make Track

You are going to develop a new class named `Track`. This class will have (at a minimum) the following elements:

* Attributes:
  * Title
  * Number
  * Duration (document units)
* Behaviors:
  * Default and at least one argumented constructor
  * Appropriate getters for attributes

This is largely an informational class. It is reasonable in the long term that this class might have a `play()` method, but for now, we can keep it to Title, number and duration.

I would highly suggest testing out `Track` to make sure all of the interfaces work __before__ moving on to the third step.

### Third Step: Integrate Track and Album

You are going to modify `Album` appropriately with your newly developed `Track` class.

Replace all relevant elements of from `Album`, integrate `Track` into the `Album` methods.

As a hint: you are going to need to change `addTrack`, both `getAlbumLength` methods and `shuffleAlbum`.

Make sure you demonstrate your new functionality in `int main`. It should be obvious your two classes `Album` and `Track` are working correctly.

Remember: If you find yourself copy/pasting code from one area of your work to another, there is likely a better way! Functionalize! 

#### Extra Credit

You are welcome to implement more functionality. Be very clear about what is deviating from the baseline. Only exceptional work will be recognized.