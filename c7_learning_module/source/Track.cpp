#include "../headers/Track.h"
#include <iostream>
#include <string>

using namespace std; 

Track::Track()
{
    title = "NA";
    number = -1; 
    duration = -1; 
}
Track::Track(string t, int n, int d)
{
    title = t;
    number = n; 
    duration = d; 
}

string Track::getTitle() const
{
    return title;
}
int Track::getNumber() const
{  
    return number; 
}

int Track::getDuration() const
{
    return duration;
}

