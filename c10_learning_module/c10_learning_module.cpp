#include <string>
#include <iostream>
using namespace std;

// Binary search to find key inside array
// Input: array - in ascending order
//        key - value we are looking for in array
//        start - location in array where we should be looking
//        end   -  last possible location to look
// Return ether: positive value - location of a key element in array
//               negative value - insertion point where key element should go
int BS(int array[], int key, int start, int end);

//Sorting array
//void sort(int arr[], int n);  

void print(int arr[], int n){
	for(int i = 0; i < n; i++){
		cout << arr[i] << " ";
	}	
	cout << endl;
}



void generate(int a[], int n){
	for(int i = 0; i < n; i++){
		a[i] = 1 + rand() % 100;
	}
	
}



void sort(int arr[], int n){
	int temp;
	for(int i = 0; i < n - 1; i++){
		int j = i + 1; 
		temp = arr[j];
			while(j > 0 && temp < arr[j-1]){
				arr[j] = arr[j - 1]; 		
				j--; 
			}	
		arr[j] = temp; 	
	}
}


char compare(int a[], int b[], int s){
	for(int i = 0; i < s; i++){
		if(a[i] != b[i]){
			return 1; 
		}
	}
	return 0; 
}  



int main(){
	const int SIZE = 100; 
    int a[SIZE]; 
	int b[SIZE];
		//  0 1 2 3 4 5 6 M 8 9 0 1 2  3  4  5 
	generate(a, SIZE); 
	generate(b, SIZE); 
	sort(a, SIZE);  
	sort(b, SIZE);
	print(a, SIZE);
	print(b, SIZE); 

	if(compare(a, b, SIZE) == 0){
		cout << "arrays are equal" << endl; 
	}
	else{
		cout << "arrays are different" << endl; 
	}
	



	
	int values[] = {1,2,3,4,5,6,6,6,6,6,7,8,8,10,12,15};
    int vSize = 16;

    cout << "Calling binary search:" << endl;
    int result = BS(values,9,0,vSize-1);
    cout << "Return of BS: " << result << endl;
}

int BS(int array[], int key, int start, int end){
    //Index of mid point in the array
    int mid = ((end - start ) / 2) + start;
    cout << "BS(array,"<<key<<","<<start <<"," << end <<") mid: " << mid << endl;

    //Base Case        
    if(key == array[mid]){
        // - found it! return mid
        return mid;
    } else if(end < start) {
        // - didn't find it, return insertion point
        return (start + 1) * -1;
    } else if (key < array[mid]) {
        // determine if you need to go left or right
        // Recursive call to take left path
        return BS(array,key,start,mid-1);
    } else if (key > array[mid]) {
        // Recursive call to take left path
        return BS(array,key,mid+1,end);
    } else {
        cout << "ERROR! Reached end of BS!" << endl;
        return '0';
    }
}
