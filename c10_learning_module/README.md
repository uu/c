# C10 Learning Module

Take what we are going to do in class and build on it.

## Evaluation
You will be graded on:
- Proper coding style and indentation
- Reasonable naming convention
- Appropriate comments, headers and useful documentation
- Best practices for encapsulation and access control
  
Remember: Functionalization is your friend! The more you can functionalize (writing small functions that are reusable), the better! 

## Instructions

In class we are going to start writing a binary search algorithm. You are welcome to take what we did in class and utilize it here.

The idea is that we are going to develop a generalized sort function that we can use in all kinds of different spaces.

_A note:_ Binary Search is a fundamental and common algorithm. I urge you be cautious when using external sources to solve this problem. 
1. You must cite your sources. 
2. This will be on the exam. You need to understand how it works and the subtlety of its implementation.

### (10 pts) Develop and write a recursive `binary_search` function (capturing in class work) 
This algorithm should take an array of integers `sorted` and a search value called the `key`.
* Your function should use `binary_search` to search the `sorted` array for the `key`
* If your function finds the `key` in the array, it should return the index of the array at which the `key` integer was found.
* If your function does NOT find the `key` in the array, it should return the negative of the index at which point the `key` would be inserted. I would suggest reading and citing the book (p. 287) or lecture slides in order to help you get started.
* I also highly suggest you try out your algorithm by hand with pen and paper before you actually code it. Make sure it will work for a variety of arrays. Start with arrays of size 0, size 1, size 2, and size 5 or 7. 

### (40 pts) Develop a second function to `sort` an unsorted array using your `binary_search` function
Write another function that takes in an `unsorted` array and utilizes your `binary_search` function to sort this unsorted input array. 
* Inside your `sort` function you will need a second array (to use as temporary working space) to hold the sorted numbers.
* For each number in `unsorted`, do a binary search on the second "working space" for that number.
* Use the insertion point return value from your `binary_search` function to know where to insert that number into the working space array. 
* When finished sorting all the values, copy the sorted values back into the `unsorted` array so that it's now in sorted order.

Hints:
* Is an array of one value sorted? (yes)
* You **need** to develop helper functions for this. As a suggestions: `insert(int a[], int len, int pos, int val))` , `copy(int src[], int dest[], int len)` and `shuffle(int a[], int len, int pos)` among others.
* Remember arrays are passed by reference, not by value. This means if you change an array in one function, it changes the array in other functions.


### (25 pts) Develop a third function that compares two un-sorted int arrays to each other and determines if they are the same. 
__The two arrays must be at least of size 100.__
* `bool compare(int a[], int b[], int len)`
* Use your `sort` function above to sort both arrays before comparing them. 
* Your function should return a `bool` that is true if the arrays are equal and false if not.
* This function should not modify the order of the two original arrays, just use sort on a temp copy of the arrays 

### (25 pts) Test Code in Main
Because your second and third functions test your binary search function, the code in `main` should do the following:

Test your second function:
* Fill an array with 100 random integers between the values of 1 and 100. 
* Pass this array of random values to your sorting function.
* After the function returns, verify that the array is indeed sorted (this will need a `verify` function). 

Test your third function:
* Fill two arrays with the _same_ 100 random integers between the values of 1 and 100, except, fill one array from left to right and the other array from right to left.
* Compare them to see if they are equal using your third function. 
* They should be equal.
* Fill two arrays (of size 100) with a total of 200 _different_ random integers between the values of 1 and 100.
* Compare them to see if they are equal using your third function. 
* The probability of them being equal is very, very, small.

Be sure to properly functionalize your program. You will need more than 'just' the functions that are required.
