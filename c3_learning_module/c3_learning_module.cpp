#include <iostream> 
#include <string> 

using std::cout; 
using std::cin; 
using std::endl;
using std::string; 

class Balloon {

    public: 
    
    //Properties of Balloon
    string color; //Color of balloon 
    int volume; //Volume of baloon
    bool popped; //if popped true or false
    
   Balloon(){ 
        
        volume = 0; 
        color = "blank"; 
        popped = false; 
    
    }
    
    Balloon(int v, string c){
        
        color = c; 
        volume = v; 
        popped = false; 

    }

    Balloon(bool pop){
        popped = pop; 
        

    }

    //Behaviors
    bool getFloat() {
        return ! popped; 
    }

    bool balloonpopped(){
        if(popped == true){
            cout << "The balloon was popped!" << endl; 
        }
    }
    
    bool balloonfloats(){
        if(popped == false){
            cout << "The balloon floats!" << endl; 
        }
    }
}; 

int main(){
    
    Balloon red(70, "Red"); 
    
    cout << "The balloons color is " << red.color << " | The balloons volume is: " << red.volume << " | Does the balloon float? " << endl;
    
    if(red.getFloat()){ 
        cout << "yes" << endl; 
    }
    else{
        cout << "no" << endl; 
    }


}
