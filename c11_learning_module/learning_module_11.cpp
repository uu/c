#include <iostream>

using namespace std; 


//Returns the sum of all prime factors of n
//Ex: sumPrimeFactors(56) : 56 = 28 * 2 = 14 * 2 * 2 = 7 * 2 * 2 * 2 Sum = 13
//Ex: sumPrimeFactors(11) : 11 = 11 Sum = 11
int sumPrimeFactors(unsigned int n, int sum, int current_factor); 

// Recursive, solves Pirates coin puzzle
// Output: Optimal value of coins collected
// Input - coins, array of integers holding the value of each coin
//       - num_coins, size of coins (size of array)
// Follows pirate's rules.
//int pt(const int coins[], const int size, int best_score, int next_index, int sum); 



//Code created under the guidance of Ethan
//Algorithm derived by Ethan
int pt(const int coins[], const int size, int best_score = 0, int next_index = 0, int sum = 0) {
    //run out of coins
    //Try picking every remaining coin

    for(int i = next_index; i < size; i++){
        //reseting sum
        int s_sum = sum;
        // picked coin 1
        //i = 0 == 2
        //i = 1 == 3
        //sum of the coins picked before
        s_sum += coins[i];

        //update best score
        if(best_score < s_sum){
            best_score = s_sum;
        }

        //base case
        if(!(i >= size - 2)){
        	best_score = pt(coins, size, best_score, i + 2, s_sum);
        // cant return a best_score taht is lower than current one
    	}
	}

 	return best_score;

}


//Checks to see if n is a prime number or not
//Input n - number to check
//Output: If number is prime or not
bool isPrime(int n){
	if(n <= 1){
		return false; //prime numbers cannot be 0 or 1 	
	}
	
	// n = 4 
	// i = 2
	// 4 % 2 == 0; 
	// if 0 then not a prime
	// else 1 = prime

	for (int i = 2; i < n; i++){
		if(n % i == 0){
			return false;
		}
	}
	return true; 

}

int main(){
	int s = sumPrimeFactors(120, 0, 2); 
	cout << "Sum: " << s << endl;

	for(int i = 10; i < 100; i++){
		cout << "Prime Factors of "  << i <<  " are: " << sumPrimeFactors(i, 0, 2) << endl; 
	}

	const int SIZE = 20;
	int coins[SIZE] = {1, 4, 1, 2};
	int coins1[SIZE] = {1, 7, 2, 9, 7, 8};
	int coins2[SIZE] = {1, 7, 4, 1, 5, 0, 6, 7, 3, 7, 6, 9, 6, 9, 5, 5, 2, 2, 6, 5};
	
	cout << "Best value of coins: " <<  pt(coins,SIZE) << endl; 
	cout << "Best value of coins: " << pt(coins1,SIZE) << endl;
	cout << "Best value of coins: " << pt(coins2,SIZE) << endl;


}

//Divide by two / check if number is prime if not divide by 2 again check if prime etc..? 

int sumPrimeFactors(unsigned int n, int sum = 0, int current_factor = 2){ 
	//base case
	if(isPrime(n) || n <= 1) {
		return sum + n; 
	}	
	else {
		//Need to factor
		//Then add facors
		//Check if 120/2
		//Update number
		//update sum
		//recurse
		if(n % current_factor == 0){ //Check if number is divisible 
			//n /= current_factor; 
			//sum += current_factor; 
			return sumPrimeFactors(n / current_factor, sum + current_factor, current_factor); //recurse 
		}
		else {
			current_factor++; //need to increment to next factor to start checking
			while(!isPrime(current_factor)){
				current_factor++; 
			} 
			return sumPrimeFactors(n, sum, current_factor);  
		}	
	}
	return -1; 
}













/* 
linear recursion 
1 2 3 4 5
foo{1234} + 5
foo{123} + 4 ^

---------------

1 5 1 5 5 1 5
      x ^ x ^
1) Get the most value
2) Can't pick up adjacent coinds


Branching recursion (not linear)


//Base Case 1
5
Value: 5

//Base case 2
1 5 
Value = 5

// Base case 3? 
Value = 5
*/



//1 3 7 9 15 20

//int piratesTreasure(int coins[], int num_coins){
	
	//int p1 = coin[n] + rc(array, n-2)    
	//int p2 = coin[n-1] + rc(array, n-3)   	
	
	//Base case 1
	




/* 
Args 
* array 
* size 
* best_score
* next_index(index of the next coin you can possibly pick)
* sum 


{1, 4, 1, 2}
		
x(0, 0, 0)
 0 (1, 0, 1)
  2(2, 2, x)
  3
 1
  3
 2
 3

 
int pt(const int coins[], const int size, int best_score, int next_index, int sum) {
	//run out of coins
	//Try picking every remaining coin 

	for(int i = next_index; i < size; i++){
		//reseting sum 
		int starting_sum = sum; 
		// picked coin 1 		
		//i = 0 == 2
		//i = 1 == 3
		//sum of the coins picked before 
		starting_sum += coins[i]; 
		
		//update best score
		if(best_score < sum){
			best_score = sum; 
		}
		
		//base case
		if(!i >= size - 2){
			return best_score; 
		} 
		
		best_score = pt(coins, size, best_score, i + 2, sum); 
		// cant return a best_score taht is lower than current one 
	}

	return best_score; 

}


        <----
1 3 7 9 15 20 R:32 
[  12  ] x  ^ (32)
[ 8 ] x  ^  x (23)

1 3 7 9 R: 12
[3] x ^ (3+9)
1 X ^ x (1+7)

1 3 7 R: 8


1 X ^ (1 + 7)
X ^ X (3)

---------------------

1 3 Base case: return largest of two (3)
x ^
^ X


1 Base case: return 1
^

int vOp1 = coin[n] + rc(array,n-2)
int vOp2 = coin[n-1] + rc(array,n-3)
*/

