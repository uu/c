# C11 Learning Module

Take what we are going to do in class and build on it.

## Evaluation
You will be graded on:
- Proper coding style and indentation
- Reasonable naming convention
- Appropriate comments, headers and useful documentation
- Best practices for encapsulation and access control
  
Remember: Functionalization is your friend! The more you can functionalize (writing small functions that are reusable), the better! 

## Instructions

You are welcome to combine these two parts into one single .cpp file.

## (40 pts) Part 1: Sum of Prime Factors

Factorization is used in many cryptographic functions. This is a simplified utility function that is representative of larger number theory algorithms used in cryptography.

Write a function `int sumPrimeFactors(int n)` that:
* returns the sum of all of the prime factors of `n`
* Any given integer can either be a prime number, or have a collection of prime factors.

Ex: sumPrimeFactors(56) :
56 = 28 * 2 = 14 * 2 * 2 = 7 * 2 * 2 * 2 
Sum = 13

Ex: sumPrimeFactors(11) :
11 = 11
Sum = 11

Demonstrate your function by finding the smallest and largest sums for two digit numbers. (the largest should probably be the largest prime in \[10-99\])

Hint: I would suggest developing an `bool isPrime(int n)` utility function (it could be recursive, but does not need to be). If you take a function from previous work, cite it. 

## (60 pts) Part 2: Pirate Coins

You are wandering through The Back 40 and come upon a small hidden sandy cove. At the back of the cove is a cave, outside of which stands a miniature pirate. In front of the pirate are a line of rare coins. Each coin is worth a different amount of money. Grinning, the pirate explains that you may take the coins you wish but with the exception that you are NOT allowed to take any coin if you have already taken another coin that was on either side of the coin you took.

For example, here is a list of coins and their values:
```
                  1 5 1 1 3
You should take:    x     x for a total value of 8 in gold.

                  1  5  6  8  7
You should take:  x     x     x for a total value of 14 in gold.
```

Your task is to write a recursive function that takes an array of coin values and returns the optimal amount of gold that can be collected.
```cpp
    // Recursive, solves Pirates coin puzzle
    // Output: Optimal value of coins collected
    // Input - coins, array of integers holding the value of each coin
    //       - num_coins, size of coins (size of array)
    // Follows pirate's rules.
    int piratesTreasure( int coins[], int num_coins ) {
        // Complete this using recursion.
    }
```
Example: 
```cpp
    int coins[5] = {1, 5, 6, 8, 7};
    cout << piratesTreasure( coins, 5 ) << endl; // This should output 14
```

Demonstrate your function on at least 3 different sized arrays.

Hints:
* What if the array contains only one coin?
* What if the array contains only two coins?
* What if the array contains only three coins?
* You should probably start at the right side of the array and work your way to the left. Use the num_coins to keep track of where you are.

