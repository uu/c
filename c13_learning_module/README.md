# C13 Learning Module

## Evaluation
You will be graded on:
- Proper coding style and indentation
- Reasonable naming convention
- Appropriate comments, headers and useful documentation
- Best practices for encapsulation and access control
  
Remember: Functionalization is your friend! The more you can functionalize (writing small functions that are reusable), the better! 

## Instructions

We are going to get started utilizing dynamic memory. I would highly suggest getting this finished before jumping into C14's work.

You are going to create a grading program. I am going to outline the requirements, but you are going to need to do some 'filling in' to make it both look good and work correctly.

### (40 pts) Dynamic Array Manipulation

You need to create a function that will manage a given array by re-allocating and adding a new value.

Use the signature: `void allocateAndAdd(float* &array, int &aSize, float newValue )`

This function is going to re-allocate array with the same values, then add the newValue to that new array. Be aware that `array` is a pointer that is passed in by reference. This means that you are going to be able to modify the value (the address array is pointing to) inside the function, so is `aSize`.  

I would suggest following these steps:
* Create a temp array (dynamically) with the new size needed
* Copy the contents of array into the temp array (I would suggest using a function!)
* Copy the new value into the correct location of the temp array
* At this point you should have two very similar arrays, one (`temp`) that is one larger
* Delete the old array
* Set array to be pointing to the same values as temp
* Update the size

Remember that `temp` will go out of scope when the function returns, so array will be holding a new memory location, with a new array that is one larger than before.

### (20 pts) `fillGrades` Function

Develop a function that collects grades from the user. Each time a grade is added, you should insert the grades using your `allocateAndAdd` method.

You should collect three different sets of grades: Learning Modules, Exams and Final Project portions into three different arrays.

You will need to think through the functions needed for all of this. Remember each function should only be 'doing' one thing.

### (20 pts) Stats

Each time a grade is entered, the user should be presented with some statistics of the current grade state: Average, Median, Min and Max.

### (20 pts) Final Presentation

Once the user is done entering values, a final grade calculation should be made. This should show the stats for the sub sections, as well as a final grade (I would suggest looking up weights from the syllabus).

As a heads up, these last three sections will largely be graded on function and style. Printing out a string of numbers without context to the screen is not what I am looking for. Spend some time thinking about what you want on the output.

### (up to 15 pts extra) Improvements

There are lots of ways to improve this beyond these specifications. If you feel you are deserving of extra credit, cite what you did and how you did it. 