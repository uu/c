#include <iostream>
#include <string> 

using namespace std; 


//Function used to copy one array and store in another
void copyArr(float* &src, float* &des, int aSize){
	for(int i = 0; i < aSize; i++){
		des[i] = src[i]; //Takes i numbers and stores into dec array
	}
}


//Incrementing array by 1 
void allocateAndAdd(float* &array, int &aSize, float newValue ){
	//Allocating new memory
	//WIll need a delete later in code 
	float* temp = new float[aSize + 1]; //making dynamic 
	
	copyArr(array, temp, aSize); //copying array into temp 
	
	temp[aSize] = newValue; 

	aSize++; //incrementing aSize
	
	delete[] array; //deleting array since we no longer are using it
   	array = temp; 

}

//Fills in grades for LM, Exams, and Projects
void fillGrades(float* &LM, float* &Exams, float* &Projects, int &LMsize, int &examSize, int &projectSize){
	int newGrade; 
		
	//LM
	cout << "Insert Learning Module Grades: "; 
	cin >> newGrade;
	allocateAndAdd(LM, LMsize, newGrade);   
	
	//Exam
	cout << "Insert Exam Grades: "; 
	cin >> newGrade;
	allocateAndAdd(Exams, examSize, newGrade);  
	
	//Final Projects:
	cout << "Insert Final Project grades: "; 	
	cin >> newGrade;
	allocateAndAdd(Projects, projectSize, newGrade);  
}

//What do I want to print out?
//Average
//Median
//Min and Max

//Calculate the average grade of LM, Exams, Projects
float average(int aSize, float array[]){
	float sum = 0.0; 
	float average = 0.0; 
	
	for(int i = 0; i < aSize; i++){
		sum += array[i]; 
	}
	average = sum / aSize;

	return average; 	
	
	cout << "The sum is: " << sum << endl; 
	cout << "The size of the array: " << aSize << endl; 

}

//http://www.cplusplus.com/forum/beginner/78445/
//Returns highest value in an array 
float max(int aSize, float array[]){
	//int min = array[0]; 
	int max = array[0];  

	for(int i = 0; i < aSize; i++){
		if(max < array[i]){
			max = array[i]; 
		}
	}
	return max; 
}

//Returns smallest value in an array
float min(int aSize, float array[]){
	int min = array[0];
	for(int i = 0; i < aSize; i++){
		if(min > array[i]){
			min = array[i]; 
		}
	}
	return min; 
}

//http://www.cplusplus.com/forum/beginner/138770/
float median(int aSize, float array[]){
	int i, j;
	int median = array[0]; 
	for (i = 0; i < aSize; i++){
		for (j = 0; j < aSize - i - 1; j++){
			if (array[j] < array[j + 1]){
				int temp = array[j];
				array[j] = array[j + 1];
				array[j + 1] = temp;
			}
		}
	}
	if (aSize % 2 == 0){
		median = (array[i/2] + array[i/2-1])/2.0;
	}
	else{
		median = array[i/2];
	}
	return median; 
}



//Print function to output stats 
void LMprint(int aSize, float LM[]){
	cout << endl;
	cout << "Learning module grades" << endl;
	cout << "--------------------------------------------------" << endl;

	cout << "The average LM grade is: " << average(aSize, LM) << endl; 
	cout << "The lowest LM grade you have recieved is: " << min(aSize, LM) << endl; 
	cout << "The highest LM grade you have recieved is: " << max(aSize, LM) << endl;  
	cout << "The median grade is: " << median(aSize, LM) << endl;  
	
	cout << endl;
}
void examPrint(int aSize, float Exams[]){
	cout << "Exam grades" << endl;
	cout << "--------------------------------------------------" << endl;
	
	cout << "The average Exam grades is: " << average(aSize, Exams) << endl; 
	cout << "The lowest Exam grade you have recieved is: " << min(aSize, Exams) << endl; 
	cout << "The highest Exam grade you have recieved is: " << max(aSize, Exams) << endl;  
	cout << "The median grade is: " << median(aSize, Exams) << endl;  
	
	cout << endl;
}

void projectPrint(int aSize, float Projects[]){
	cout << "Project grades" << endl;
	cout << "--------------------------------------------------" << endl;
	
	cout << "The average project grade is: " << average(aSize, Projects) << endl; 
	cout << "The lowest project grade you have recieved is: " << min(aSize, Projects) << endl; 
	cout << "The highest project grade you have recieved is: " << max(aSize, Projects) << endl;  
	cout << "The median grade is: " << median(aSize, Projects) << endl;  
	
	cout << endl;
}

//Prints final grade
//takes LM, Exams, Projects and averages them
void finalPrint(float* &LM, float* &Exams, float* &Projects, int &LMsize, int &examSize, int &projectSize){
	 cout << "The final grade is: " << (average(LMsize, LM) + average(examSize, Exams) + average(projectSize, Projects) / 3) << endl;  
}

int main(){
	
	//Init arrays to 1 
	int LMsize = 1; 
	int projectSize = 1; 
	int examSize = 1; 
	
	//varibles for input 
	int grade;
    int gradeAmount; 	
	
	
	float* LM = new float[LMsize];
	float* Projects = new float[projectSize]; 
	float* Exams = new float[examSize]; 

	//Need to fill the first element in the array with a value
 	//fillGrades(LM, Projects, Exams, aSize);
	cout << "What is the first learning module grade you want to input: "; 
	cin >> grade; 
	LM[0] = grade; 

	cout << "What is the first Project grade you want to input: "; 
	cin >> grade; 
	Projects[0] = grade; 

	cout << "What is the first exam grade you want to input: "; 
	cin >> grade; 
	Exams[0] = grade; 
	
	
	cout << "How many grades will you be entering?: "; 
	cin >> gradeAmount;
    cout << endl; 	
	
	for(int i = 0; i < gradeAmount; i++){  
		fillGrades(LM, Exams, Projects, LMsize, examSize, projectSize);
	}
	
	
	LMprint(LMsize, LM); 	
	projectPrint(projectSize, Projects); 
	examPrint(examSize, Exams); 
	finalPrint(LM, Exams, Projects, LMsize, examSize, projectSize); 
}
