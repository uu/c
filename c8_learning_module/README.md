# C8 Learning Module

Wrapping up Classes, I want you to make two more advanced classes.

## Evaluation
You will be graded on:
- Proper coding style and indentation
- Reasonable naming convention
- Appropriate comments, headers and useful documentation
- Best practices for encapsulation and access control

## Instructions

You should be pros at making classes at this point. You are free to approach these two classes in whatever way you want, but make sure you follow appropriate style around separation of files, access control and `const`.

You need to develop each class (30 pts each) and fully demonstrate / test each class (20 pts each).

### Class 1: Stopwatch

This will be a helpful tool to use in the future when we start talking about code efficiency. You are going to make a `Stopwatch` class that will record elapsed duration between calls to `start()` and `stop()`. This class will also cleanly display the relevant information to the user.

This class must implement:
* `start()` – a method for setting the start of the stop watch
* `stop()` – a method for stopping the watch
* `getElapsedTime()` – a method that returns the elapsed time between start and stop in seconds
* `printWatch()` – a method that will output to the terminal the current status of the watch

I would highly suggest you look over <ctime> in particular clock() as a way to measure time. You are more than welcome to implement other methods, but any method you use must be documented 

Your test program can be based on user input (time between entries) and/or amount of time it takes to execute a portion of code (really big, and nested for loops take a long time). 

If you are testing user input, you will need to create three stopwatches: One that starts when the program begins and stops when the program is about to end and two that are set by waiting on the user. The total time from the two user based stopwatches should relate to the total elapsed time of the first stopwatch.

If you are going to time portions of code, you are going to need to test code that takes at least 10 seconds to run, and you should be able to show that using different parameters results in different time.

#### Extra Credit: 5pts
Implement a stopwatch accurate down to the millisecond. It must be able to display both seconds and milliseconds as two different methods.

### Class 2: Fibonacci Number

You will be creating a `Fibonacci` class that represents a fibonacci number in sequence.

Fibonacci numbers follow a pattern of f(n) = f(n-1) + f(n-2), starting with f(0) = 0 and f(1) = 1. Thus the sequence would be 0 1 1 2 3 5 8 13 21 ...

You will need to implement:
* Default constructor that starts at f(1)
* `incrament()` - Steps current fibonacci number one step 'forward' in the pattern. So a value of 3 would change to be value 5.
* `getValue()` - Returns the current fibonacci number as an int
* `getIndex()` - Returns the position of the current number. If the value was 13 this should return 7

Your test should at least print out the first 30 numbers. Formatted so that 5 are on each line. This will require a loop, probably 2.
You will need to demonstrate your other methods as well.

#### Extra Credit: 5 pts
Implement a `decrament()` method to step 'backwards' in the series.
