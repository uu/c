#include <iostream>
#include "../headers/stopwatch.h"
#include <ctime>
#include <math.h>
using namespace std;

int frequency_of_primes (int n) {
  int i,j;
  int freq=n-1;
  for (i=2; i<=n; ++i) for (j=sqrt(i);j>1;--j) if (i%j==0) {--freq; break;}
  return freq;
}



int main() {
  int num;  
  int f; 
  Stopwatch s1; 
  
  s1.start();
  
  cout << "What is your favorite number?: "; 
  cin >> num; 
  cout << "You favorite number is " << num << endl;
  
  for(int i = 0; i < 99999; i++){
    cout << "a" << endl;
  }


  f = frequency_of_primes(99999);

  s1.stop(); 
  s1.getElapsedTime(); 
  s1.printWatch(); 



}
