#include <iostream>
#include <ctime> 
#include "../headers/stopwatch.h"
#include <math.h> 

using namespace std; 

Stopwatch::Stopwatch(){
  seconds = 0; 
  time = 0; 
}

/*
Stop::Stopwatch(int timer){
  timer = time; 
}
*/

int Stopwatch::start(){
  t = clock(); 

} 

int Stopwatch::stop(){ 
  t = clock() - t; 

}

float Stopwatch::getElapsedTime(){
  time = ((float)t) / CLOCKS_PER_SEC;
  return time; 
}

void Stopwatch::printWatch() const {
  cout << "Total Time: " << time << endl; 
}
