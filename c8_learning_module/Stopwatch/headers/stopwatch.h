#ifndef STOPWATCH_H_
#define STOPWATCH_H_


#include <string>
#include <iostream>
#include <ctime> 


class Stopwatch {
    private: 
      float seconds;
      float time; 
      clock_t t; 

    public: 
        Stopwatch(); 

        int start(); 
        int stop();

        float getElapsedTime(); 

        void printWatch() const; 



};

#endif
