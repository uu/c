#include <iostream>
#include "../headers/fib.h"

using namespace std; 

Fib::Fib(){
  currentFib = 1; 
  previousFib = 0;
  index = 1; 
}


int Fib::incrament(){
  int total; 
  total = currentFib + previousFib;
//  3   =    2       +   1
  currentFib = total; 
  previousFib = currentFib - previousFib; 
  index = index + 1; 
}

int Fib::getValue(){
  return currentFib;
}

int Fib::getIndex(){
  return index; 
}

void Fib::print() const{
  cout << "The fib number is: " << currentFib << endl;
  //cout << index << endl; 
}
