#ifndef FIB_H_
#define FIB_H_

class Fib {

  private: 
    int currentFib; 
    int previousFib; 
    int index; 


  public: 
    
    Fib(); 

    int incrament(); 

    int getValue(); 

    int getIndex(); 


    void print() const; 

};



#endif
