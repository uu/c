///////////////////////////////////////////////////////////////////////////////////////
//
// Name: Saned Gharari
// Date: 11/04/2020
// Course Code: CS-172-1
//
// License: CS172 students at whitworth may edit this file but not share this 
// file electronically with other students.
//
// Copyright (C) Saned Gharari 2020 - 2021
//
//  Originally developed and outlined by Scott Griffith and Kent Jones (4/2020)
//
////////////////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include "../headers/CS172Vector.h"
using namespace std;

int main()
{
    //TODO:: Replace the following code with your own (complete) demonstration of your MyVector
    CS172Vector<float> grades;

    
    grades.push_back(56.4); 
    grades.push_back(87.1); 
    grades.push_back(87.7); 
    grades.push_back(87.2); 
    grades.push_back(34.45); 
    grades.push_back(56.76); 
    grades.push_back(76.12); 
    grades.push_back(92.66); 
    grades.push_back(52.45); 


    cout << "Currently you have " << grades.size() << " total grades." << endl; 

    cout << "You dropped a class - grade was removed" << endl;
    grades.pop_back(); 

    cout << "You added Calculus 2 - grade was added" << endl; 
    grades.insert(65.3, 8);    

    cout << "Looks like its the end of the semester, time to clear your grades!" << endl; 
    //grades.clear();

    if(grades.empty() == 1){
        cout << "Gradebook is empty" << endl; 
    }
    else{
        cout << "Gradebook is still full" << endl;
    }
   
    /*
    CS172Vector<int> testVec;

    testVec.push_back(00);
    testVec.push_back(10);
    testVec.push_back(20);
    testVec.push_back(30);
    testVec.push_back(40);
    */
   
    cout << "Print elements" << endl;
    for (int i = 0; i < grades.size(); i++)
    {
        cout << "Grades[" << i << "]: " << grades[i] << endl;
    }
}