# C16 Learning Module

## Evaluation
You will be graded on:
- Proper coding style and indentation
- Reasonable naming convention
- Appropriate comments, headers and useful documentation
- Best practices for encapsulation and access control
  
Remember: Functionalization is your friend! The more you can functionalize (writing small functions that are reusable), the better! 

## Instructions

You are going to create a `CS172Vector`. It is going to be very similar to `std::vector` but a little simplified.

The name `CS172Vector` is a little important. You cannot use `vector` if you are also `using namespace std;` there will be a definition conflict. You can get into the weeds of defining a namespace, but that is not inside the scope or expectations of this class.

### (60 pts) Develop `CS172Vector`

I am providing you with a scaffold for you to use. You are welcome to modify it, but all the required modifications are noted  with `//TODO::` tags. There are 15 of them in `CS172Vector.cpp`.

__There may be mistakes! And/or you may take a different approach than I did in the set up!__ That is fine! You can fix any mistakes I introduce, or you can (reasonably) modify things to work the way that makes sense for you.

Remember: the placement of `CS172Vector.cpp` in the `./headers/` directory is weird, but intentional. This is a way we can pull the implementation out of the definition of the `CS172Vector.h` while still using templates. __You should not need to move any files!__

I would highly suggest making small changes and testing them. Many of the things may interact with each other. Make baby steps and work up from there.

### (40 pts) Testing

Develop a novel and interesting program to test your vector. It should use vectors of at least two types. It should also show off the demonstration of __all__ your methods.

If you don't want to think of something new, you can re-form your grade program to worth with vectors in place of dynamic arrays.

__REMEMBER__: It is a serious academic integrity violation to copy code electronically or share your code electronically with another student! 

### (up to 15 pts Extra Credit) Double Pointer array

There are some functional benefits to using a double pointer inside of your vector. You can control when things are allocated and when they are not. But it also comes with a complication in managing the double pointers.

To get this extra credit you will need to use `Type** data` as the central place of data in your container. Show this works.