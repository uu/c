///////////////////////////////////////////////////////////////////////////////////////
//
// Name: Saned Gharari
// Date: 11/04/2020
// Course Code: CS-172-1
//
// License: CS172 students at whitworth may edit this file but not share this 
// file electronically with other students.
//
// Copyright (C) Saned Gharari 2020 - 2021
//
//  Originally developed and outlined by Scott Griffith and Kent Jones (4/2020)
//  Working with Seth
////////////////////////////////////////////////////////////////////////////////////////

#include "../headers/CS172Vector.h"
#include <iostream> 

//Utility function to re-size the vector
// input: new_size - size of new capacity
// post:  size of vector will now be new_size
//        if new_size < cap, data will be lost
template <typename Type>
void CS172Vector<Type>::resize(int new_size){
    //TODO:: Allocate a new temp data space
    Type* temp = new Type[new_size]; 
    //TODO:: Pull everything over from data into the new space
    for(int i = 0; i < new_size; i++){
        temp[i] = data[i]; 
    }
    //TODO:: Delete our (about to be stale) data
    delete[] data; 
    //TODO:: Update data, cap and num as necessary
    data = temp; 
    cap = new_size;  
    if (new_size > cap)
    {
        num = new_size; 
    }
}


// Basic Constructor
// Empty vector with cap = 8
template <typename Type>
CS172Vector<Type>::CS172Vector(): CS172Vector(8) {
    //Basic Initialization
    //Should not need anything here, relies on the other contructor
}

// Size Constructor
// Allocates a s number of spaces
// Still empty
template <typename Type>
CS172Vector<Type>::CS172Vector(int s){
    //TODO:: Implement the sized constructor for the Vector using the following steps
    // 1) Initialize the properly sized dynamic array
    // 2) Make sure contents of data are set to default constructors
    // 3) Update internal size variables: num / cap
    data = new Type[s]; 
    cap = s; 
    num = 0;
}

// Initialization constructor
// Allocates s number of spaces
// all filed with init value
template <typename Type>
CS172Vector<Type>::CS172Vector(int s, Type init) : CS172Vector(s) {
    //TODO:: Implement the fill constructor for the Vector using the following steps:
    // 1) Keep filling vector with init until it is full
    for (int i = 0; i < cap; i++)
    {
        data[i] = init; 
    }
}

//Destructor
template <typename Type>
CS172Vector<Type>::~CS172Vector(){
    //TODO:: Dealocate dynamic memory!
    delete[] data; 
}

// Adds element to the end of the vector
// input: item - element to add to the end of the vector
// post: num incraments by one
//       re-allocation possible if full
template <typename Type>
void CS172Vector<Type>::push_back(const Type & item){
    //Check to make sure we don't need to re-size
    if( (num + 1) >= cap){
        resize(cap*2);
    }

   //TODO:: Add element to the end of data
    data[num] = item; 
    //TODO: Modify num to reflect new item
    num++; 
}

// Removes last element in vector
// post: num decrements by one
//       no need to re-size (vector space will never reduce)
template <typename Type>
void CS172Vector<Type>::pop_back(){
    //TODO:: Remove the last item in the vector
    // Hint: one instruction needed. (it is not a delete)
    data[num - 1] = NULL;      
}

// Returns the number of valid elements in the vector
template <typename Type>
int CS172Vector<Type>::size(){
    //TODO:: Return the proper value
    return num; 
}

// Returns the number of memory spaces allocated
template <typename Type>
int CS172Vector<Type>::capacity(){
    //TODO:: Return the proper value
    return cap; 
}

// Returns F if there are elements in the array
// Returns T if there are no elements in the array
template <typename Type>
bool CS172Vector<Type>::empty(){
    //TODO:: Return the proper value
    if(num <= 0){
        return true; 
    }
    else{
        return false; 
    }
}

// Removes all elements in the vector
// post: number of elements will be zero
//       no re-size
template <typename Type>
void CS172Vector<Type>::clear(){
    //TODO:: One instruction to clear out the vector
    // Hint: similar to pop_back()
    for (int i = 0; i < num; i++)
    {
        data[i] = NULL; 
    }
}

// Adds element to a specific place in a vector
// input: item - element to add
//        pos  - position of insertion
// post: Vector size increases
//       elements after pos are shifted
//       item added 
template <typename Type>
void CS172Vector<Type>::insert(const Type & item, int pos){
    //Check to make sure we don't need to re-size
    if( (num + 1) >= cap){
        resize(cap*2);
    }

    //TODO:: Shift all elements from pos to end over one
     for (int i = num - 1; i >= pos; i--){
         data[i + 1] = data[i];
     }
        
    //TODO:: Add element to pos
    data[pos] = item;
    //TODO: Modify num to reflect new item
    num++; 
}

////////////////////////////////////////////
//                                        //  
// Methods you should not have to modify! //
//                                        //
////////////////////////////////////////////

//Copy constructor
template <typename Type>
CS172Vector<Type>::CS172Vector(const CS172Vector & other){
    //Start deep copy
    this->num = other.num;
    this->cap = other.cap;

    // This is possibly inefficient
    // This could re-size to num, not cap
    // Edge case is other has lots of capacity, but very few internal objects
    this->data = new Type[cap];

    //Copy all elements over
    for(int i = 0; i < this->cap; i++){
        if(i < num){
            //Allocate new copy of data
            data[i] = other.data[i];
        } else {
            //beyond valid data, fill with null
            data[i] = Type();
        }
    }
}

// Accessor of the first element of the vector
// Same thing as vector[0];
template <typename Type>
Type CS172Vector<Type>::front(){
    if(num > 0){
        return data[0];
    } else {
        //Really this should throw an exception
        return Type();
    }

}

// Accessor of the last valid element of the vector
// Same thing as vector[size() - 1];
template <typename Type>
Type CS172Vector<Type>::back(){
    if(num > 0){
        return data[num-1];
    } else {
        //Really this should throw an exception
        return Type();
    }
}

// Assignment overload
// Performs deep copy of other into this
template <typename Type>
CS172Vector<Type>& CS172Vector<Type>::operator=(const CS172Vector<Type> & other){
    //Copy other into new memory space(deep copy)
    CS172Vector<Type> temp(other);

    //Swap out this and temp
    swap(temp);

    return *this;
    // data previously contained in this will be held by temp
    // Temp will go out of scope and destruct when function returns
}

// Utility to do a shallow swap of two vectors
// Input - other: vector that should be swapped
// Post: contents of this will switch places with other
template <typename Type>
void CS172Vector<Type>::swap(CS172Vector<Type> & other){
    std::swap(this->num, other.num);
    std::swap(this->cap, other.cap);
    std::swap(this->data, other.data);
}

// Subscript operator 
// Returns element at pos
// Copies the functionality of array[]
// Will return default Type if pos > num
template <typename Type>
Type CS172Vector<Type>::operator[](int pos){
    //Check to make sure pos is valid
    if( (pos < 0) || (pos >= num) ){ 
        //Out of num bounds, return default        
        return Type();
    } else {
        //Return value in data
        return data[pos];
    }
}