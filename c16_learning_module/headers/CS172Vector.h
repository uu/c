///////////////////////////////////////////////////////////////////////////////////////
//
// Name: Saned Gharari
// Date: 11/04/2020
// Course Code: CS-172-1
//
// License: CS172 students at whitworth may edit this file but not share this 
// file electronically with other students.
//
// Copyright (C) Saned Gharari 2020 - 2021
//
//  Originally developed and outlined by Scott Griffith and Kent Jones (4/2020)
//
////////////////////////////////////////////////////////////////////////////////////////

#ifndef CS172VECTOR_H
#define CS172VECTOR_H

template <typename Type>
class CS172Vector{
    private:
        Type *data;     //Collection of the dynamic memory
        int num;        //Total number of elements in the vector
        int cap;        //Total allocated memory of the vector

        //Utility function to re-size the vector
        // input: new_size - size of new capacity
        // post:  size of vector will now be new_size
        //        if new_size < cap, data will be lost
        void resize(int new_size);

    public:
        // Basic Constructor
        // Empty vector with cap = 8
        CS172Vector();

        // Size Constructor
        // Allocates a s number of spaces
        // Still empty
        CS172Vector(int s);

        // Initialization constructor
        // Allocates s number of spaces
        // all filed with init value
        CS172Vector(int s, Type init);

        //Destructor
        ~CS172Vector();

        // Adds element to the end of the vector
        // input: item - element to add to the end of the vector
        // post: num incraments by one
        //       re-allocation possible if full
        void push_back(const Type & item);

        // Removes last element in vector
        // post: num decrements by one
        //       no need to re-size (vector space will never reduce)
        void pop_back();

        // Returns the number of valid elements in the vector
        int size();

        // Returns the number of memory spaces allocated
        int capacity();

        // Returns F if there are elements in the array
        // Returns T if there are no elements in the array
        bool empty();

        // Removes all elements in the vector
        // post: number of elements will be zero
        //       no re-size
        void clear();        

        // Adds element to a specific place in a vector
        // input: item - element to add
        //        pos  - position of insertion
        // post: Vector size increases
        //       elements after pos are shifted
        //       item added 
        void insert(const Type & item, int pos);
        
        ////////////////////////////////////////////
        //                                        //  
        // Methods you should not have to modify! //
        //                                        //
        ////////////////////////////////////////////

        //Copy constructor
        CS172Vector(const CS172Vector & other);

        // Accessor of the first element of the vector
        // Same thing as vector[0];
        Type front();

        // Accessor of the last valid element of the vector
        // Same thing as vector[size() - 1];
        Type back();

        // Assignment overload
        // Performs deep copy of other into this
        CS172Vector<Type>& operator=(const CS172Vector<Type> & other);

        // Subscript operator 
        // Returns element at pos
        // Copies the functionality of array[]
        // Will return default Type if pos > num
        Type operator[](int pos);

        // Utility to do a shallow swap of two vectors
        // Input - other: vector that should be swapped
        // Post: contents of this will switch places with other
        void swap(CS172Vector<Type> & other);
};

// This needs to be here so the template <typename Type> from this file caries over to the .cpp file
// The Types need to be the same for the template to work
#include "../headers/CS172Vector.cpp"

#endif