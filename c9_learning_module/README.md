# C9 Learning Module

Get your coding hands dirty with some recursion!

## Evaluation
You will be graded on:
- Proper coding style and indentation
- Reasonable naming convention
- Appropriate comments, headers and useful documentation
- Best practices for encapsulation and access control
  
Remember: Functionalization is your friend! The more you can functionalize (writing small functions that are reusable), the better! 

## Instructions

You are going to develop the following three recursive functions. The following functions can all be part of the same file if you would prefer. Be sure to include good function headers to identify what each of them are doing.

Your main should call each function a minimum of three times to demonstrate the function working with different initial arguments.

These functions are roughly in order of difficulty. 
* Start with the first one and work your way down the list. 
* These can be tricky the first time you start coding. 
* Get started early and write the algorithm first! 
* Don't guess and check. (it is really hard to do this!)

## (30 pts) Recursively sum all digits of a string
Write a recursive function that has the given signature. This function should take a string of digits like this "11223390" and returns an integer that is the sum of the digits (21).
* NOTE: You may assume that the function will ONLY receive valid strings (strings that only contain digits)
* Use the `std::stoi()` function to covert each digit from a string to an integer value.
```cpp
    // Recursive, Sums up digits from string to int 
    // Input: digits, string of numeric digits (no alphabetic characters!)
    // Returns sum of numbers in digits
    int sumDigitsOfString( string digits ) {
        // Complete this using recursion.
    }
```
### Example: 
```cpp
    int sum = sumDigitsOfString( string("421421") ); // sum will contain 14
```
## (30 pts) Recursive Pyramid Generation
Write a recursive function that has the given signature. This function should take a number `n` and a character `c` and returns a string that, when output, forms a pyramid `n` levels deep of character `c`.
```cpp
    // Recursive, pyramid generator
    // Input - n, number of levels to the pyramid
    //         c, character to make up pyramid
    // Output - string made up of c characters in the shape of a pyramid n levels
    string generatePyramid( int n, char c ) {
        // Complete this using recursion.
    }
```
### Example: 
```cpp
    string aPyr = generatePyramid(5,'X'); 
    cout << aPyr;
    // X
    // X X
    // X X X
    // X X X X
    // X X X X X
```
The justification is not important (left/center/right), pick one and go with it.

## (40 pts) Triangle Numbers
Write a recursive function to compute a single value in the _Triangular sequence_. This function will take in a number `n` and will return the `n`th number from the _Traiangular sequence_. This sequence represents the number of dots in a triangle, when stacked as an equilateral triangle. 

<img src="./six_triangular_numbers.png" alt="Triangular_sequence (1,3,6,10,15,21" width="220"/>  

source: https://en.wikipedia.org/wiki/Triangular_number

* `triangle(3)` should return 6
* Hint: you can use your `generatePyramid()` and a string count to verify this function works 

You will also write a `printTriangle` function that will **iteratively** call the recursive function such that you print out all of the _triangle numbers_ from 0 to n.

```cpp
    // Recursive, finds the nth number from the triangle sequence
    // Input  - n, number in sequence
    // Return - single value, number of dots in triangle
    int triangle( int n ) {
        // Complete this using recursion.
    }

    // Prints out triangle numbers
    // Utilizes triangle() recursive function
    // Input - n, upper limit of sequence to print
    // Post Condition - Print out to the screen triangle sequence from 0 to n
    void printTriangle(int n){
        // ...
        triangle(v);
        // ...
    }
```
### Example: 
```cpp
    printTriangle(1); // prints out: 1
    printTriangle(2); // prints out: 1 3
    //  :               :
    printTriangle(5); // prints out: 1 3 6 10 15 
```