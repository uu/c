#include <iostream> 
#include <string>

using namespace std; 

// Recursive, Sums up digits from string to int 
// Input: digits, string of numeric digits (no alphabetic characters!)
// Returns sum of numbers in digits
int sumDigitsOfString(string digits, int index, int sum); 


// Recursive, pyramid generator
// Input - rows, number of levels to the pyramid
//         c, character to make up pyramid
// Output - string made up of c characters in the shape of a pyramid n levels
string generatePyramid(string current, int rows, char c, int currentrow_number); 


int triangle(int n); 


int main(){
	string digits = "456"; 
	
	int sum = sumDigitsOfString(digits, 2, 0); 
	
	cout << "The sum is: " << sum << endl; 


	string p = generatePyramid("",5 , 'X', 1); 
	cout << p << endl;

	int t = triangle(5);
	cout << t << endl;

}


/*
    string aPyr = generatePyramid(5,'X'); 
    cout << aPyr;
    X
    X X
    X X X
    X X X X
    X X X X X
*/
string generatePyramid(string current, int totalrows, char c, int currentrow_number){
	if(currentrow_number == totalrows){
		current.pop_back(); 
		return current; 
	}
	else{
		string row;	
		for(int i = 0; i < currentrow_number; i++){
			row += c;
		   	row += " "; 	
		} 
	
		current += row;
		current += '\n';
		
		return generatePyramid(current, totalrows, c, currentrow_number + 1);  
	}
}


int sumDigitsOfString(string digits, int index, int sum){
	//Base Case
	if(digits == ""){
		return sum; 	
	}
	else{
		//https://www.techiedelight.com/convert-char-to-string-cpp/
		string digit(1, digits[index]);
		sum += stoi(digit);
	       	digits.pop_back(); 
		
		return sumDigitsOfString(digits, index - 1, sum);
	}
}


int triangle(int n){
	if (n == 0){
		return 0; 
	}
	else{
		int sum = 0; 
		for(int i = 0; i <= n; i++){
			sum += i; 
		}
		return sum; 
	}	

}






